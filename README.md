# Discourse theme

To install this theme, go to Discourse admin preferences in themes section.  
Import theme from the Git repository doesn't work (js errors) so you need to import all files one by one.

Do not forget to set « default » as default theme and check that the theme has childs elements. All nested elements are in this repository.
